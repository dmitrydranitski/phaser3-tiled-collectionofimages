let controls;

export default class MainScene extends Phaser.Scene {
  constructor() {
    super({key: 'MainScene'});
  }

  preload() {
    this.load.tilemapTiledJSON('map', 'assets/tilemaps/maps/main.json');
  }

  create() {
    const map = this.make.tilemap({key: 'map'});

    const layer = map.createStaticLayer(0);
    layer.setScrollFactor(0.5);
    layer.setAlpha(0.75);

    const cursors = this.input.keyboard.createCursorKeys();
    const controlConfig = {
      camera: this.cameras.main,
      left: cursors.left,
      right: cursors.right,
      up: cursors.up,
      down: cursors.down,
      speed: 0.5,
    };
    controls = new Phaser.Cameras.Controls.FixedKeyControl(controlConfig);
  }

  update(delta) {
    controls.update(delta);
  }
}
